# PNG Combination

Creates images in PNG format by generating all possible combinations from several PNG images.

# How to use

## Send parameters 

java -jar png_combinator.jar "/home/bzx/SomeFolder/"

## Input data folder

Example of input data:
```
/home/bzx/SomeFolder/1/drawing.png
/home/bzx/SomeFolder/2/variant_1.png
/home/bzx/SomeFolder/2/variant_2.png
/home/bzx/SomeFolder/3/finition_1.png
/home/bzx/SomeFolder/3/finition_2.png
/home/bzx/SomeFolder/3/finition_3.png
```

The images you put in the folder "1" will be the background. 
The images you put in the folder with the highest number will be the foreground. 


Result:
```
/home/bzx/SomeFolder/output/drawing_variant_1_finition_1.png
/home/bzx/SomeFolder/output/drawing_variant_1_definition_2.png
/home/bzx/SomeFolder/output/drawing_variant_1_finish_3.png
/home/bzx/SomeFolder/output/drawing_variant_2_finition_1.png
/home/bzx/SomeFolder/output/drawing_variant_2_finishing_2.png
/home/bzx/SomeFolder/output/drawing_variant_2_finishing_3.png
```