import org.apache.commons.io.FilenameUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicReference;

public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            throw new IllegalArgumentException("Error: Please specify the main directory. The output directory will be created in its parent folder");
        }
        File layersDirectory = getMainDirectory(args);
        File outputDirectory = getOutputDirectory(layersDirectory);

        //to save each subfolder representing layers
        TreeMap<Integer, File> layers = buildLayersMap(layersDirectory);

        //For height width uniformity check
        AtomicReference<Integer> height = new AtomicReference<>(null);
        AtomicReference<Integer> width = new AtomicReference<>(null);
        final HashMap<String, BufferedImage> previousLayerImagePerFileMap = new HashMap<>();
        layers.forEach((integer, layerDirectory) -> {
            HashMap<String, BufferedImage> currentLayerImagePerFileNameMap = buildCurrentLayerImageMap(height, width, layerDirectory);
            if (previousLayerImagePerFileMap.isEmpty()) {
                previousLayerImagePerFileMap.putAll(currentLayerImagePerFileNameMap);
            } else {
                HashMap<String, BufferedImage> newLayerImagePerFileNameMap = new HashMap<>();
                previousLayerImagePerFileMap.forEach((previousName, previousImage) -> {
                    currentLayerImagePerFileNameMap.forEach((currentName, currentImage) -> {
                        String newName = previousName + "_" + currentName;
                        BufferedImage newImage = copyBufferedImage(previousImage);
                        for (int x = 0; x < width.get(); ++x) {
                            for (int y = 0; y < height.get(); ++y) {
                                //We do not override previous layer pixels with new layer transparent pixels
                                Color pixelColor = new Color(currentImage.getRGB(x, y), true);
                                if (pixelColor.getAlpha() == 0) continue;

                                //Override pixels
                                newImage.setRGB(x, y, currentImage.getRGB(x, y));
                            }
                        }
                        newLayerImagePerFileNameMap.put(newName, newImage);
                    });
                });
                previousLayerImagePerFileMap.clear();
                previousLayerImagePerFileMap.putAll(newLayerImagePerFileNameMap);
            }
        });
        previousLayerImagePerFileMap.forEach((finalName, finalImage) -> {
            File resultImageFile = new File(outputDirectory, finalName + ".png");
            try {
                ImageIO.write(finalImage, "png", resultImageFile);
            } catch (IOException e) {
                throw new IllegalStateException("Could not write resulting image \"" + resultImageFile.getName() + "\" in output directory.", e);
            }
        });
    }

    private static File getOutputDirectory(File layersDirectory) {
        final File parentFile = layersDirectory.getParentFile();
        File outputDir = new File(parentFile, "output");
        if (!outputDir.exists()) {
            outputDir.mkdir();
        } else {
            for (File file : outputDir.listFiles()) {
                file.delete();
            }
        }
        return outputDir;
    }

    private static File getMainDirectory(String[] args) {

        File mainDirectory = new File(args[0]);
        if (!mainDirectory.exists() || !mainDirectory.isDirectory()) {
            throw new IllegalArgumentException("Error: the main directory \"" + args[0] + "\" does not exist.");
        }
        return mainDirectory;
    }

    private static TreeMap<Integer, File> buildLayersMap(File layersDirectory) {
        TreeMap<Integer, File> layers = new TreeMap<>(Integer::compare);

        for (File layerFolder : Objects.requireNonNull(layersDirectory.listFiles())) {
            if (!layerFolder.isDirectory()) {
                if (!FilenameUtils.getExtension(layerFolder.getName()).equalsIgnoreCase("png")) {
                    continue;
                }
                throw new IllegalArgumentException("Error: the main directory must contain only directories.");
            }
            try {
                Integer layerIndex = Integer.parseInt(layerFolder.getName());
                layers.put(layerIndex, layerFolder);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Error: the layers directories name must be an integer (" + layerFolder.getName() + ").");
            }

        }
        return layers;
    }

    private static void checkHeightAndWidth(AtomicReference<Integer> previousHeight, AtomicReference<Integer> previousWidth, File file, File imageFile, int height, int width) {
        if (previousHeight.get() == null) {
            previousHeight.set(height);
        } else if (!previousHeight.get().equals(height)) {
            throw new IllegalArgumentException("All images must have the same height (" + previousHeight.get() + "px) but the image \"" + file.getName() + File.separator + imageFile.getName() + "\" have a height of " + height + "px.");
        }
        if (previousWidth.get() == null) {
            previousWidth.set(width);
        } else if (!previousWidth.get().equals(width)) {
            throw new IllegalArgumentException("All images must have the same height (" + previousWidth.get() + "px) but the image \"" + file.getName() + File.separator + imageFile.getName() + "\" have a height of " + width + "px.");
        }
    }

    private static HashMap<String, BufferedImage> buildCurrentLayerImageMap(AtomicReference<Integer> previousHeight, AtomicReference<Integer> previousWidth, File layerDirectory) {
        HashMap<String, BufferedImage> currentLayerImagePerFileNameMap = new HashMap<>();
        for (File imageFile : Objects.requireNonNull(layerDirectory.listFiles())) {
            if (!FilenameUtils.getExtension(imageFile.getName()).equalsIgnoreCase("png")) {
                continue;
            }
            try {
                BufferedImage bufferedImage = ImageIO.read(imageFile);
                int height = bufferedImage.getHeight();
                int width = bufferedImage.getWidth();
                checkHeightAndWidth(previousHeight, previousWidth, layerDirectory, imageFile, height, width);
                currentLayerImagePerFileNameMap.put(FilenameUtils.getBaseName(imageFile.getName()), bufferedImage);
            } catch (IOException e) {
                throw new IllegalArgumentException("image \"" + layerDirectory.getName() + File.separator + imageFile.getName() + "\" could not be read as a png.");
            }
        }
        return currentLayerImagePerFileNameMap;
    }

    private static BufferedImage copyBufferedImage(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPreMultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPreMultiplied, null);
    }
}
